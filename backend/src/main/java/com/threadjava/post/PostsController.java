package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.http.ResponseEntity;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) Boolean hideOwnPosts,
                                 @RequestParam(required = false) Boolean likedByMe) {
        return postsService.getAllPosts(from, count, userId, hideOwnPosts, likedByMe);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @PutMapping("/update")
    public PostUpdationResponseDto update(@RequestBody PostUpdationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.update(postDto);
        template.convertAndSend("/topic/updated_post", item);
        return item;
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> post(@PathVariable UUID id) {
        postsService.delete(id);
        //template.convertAndSend("/topic/deleted_post", "Post was deleted");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
