import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdatePost = ({
  post,
  updatePost,
  uploadImage,
  close
}) => {
  const {
    id,
    image,
    body
  } = post;

  const [updateBody, setBody] = useState(body);
  const [updateImage, setImage] = useState({ ImageId: image?.id, imageLink: image?.link });
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    if (!updateBody) {
      return;
    }
    await updatePost({ id, imageId: updateImage?.imageId, imageLink: updateImage?.imageLink, body: updateBody });
    await close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleUpdatePost}>
        <Form.TextArea
          name="updateBody"
          value={updateBody}
          onChange={ev => setBody(ev.target.value)}
        />
        {updateImage?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="updateImage" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Update</Button>
      </Form>
    </Segment>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatePost;
