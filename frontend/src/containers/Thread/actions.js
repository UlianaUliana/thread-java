import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const deletePostAction = post => ({
  type: DELETE_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);//
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = request => async (dispatch, getRootState) => {
  const updatedPost = await postService.updatePost(request);
  const mapUpdatedPost = post => ({
    ...post,
    body: request.body,
    image: { id: request.imageId, link: request.imageLink }
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedPost.id ? post : mapUpdatedPost(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === updatedPost.id) {
    dispatch(setExpandedPostAction(mapUpdatedPost(expandedPost)));
  }
};

export const deletePost = postId => async dispatch => {
  const deletedPost = await postService.deletePost(postId);
  dispatch(deletePostAction(deletedPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const reaction = await postService.getPostReaction(postId);
  const result = await postService.likePost(postId);
  const likesDiff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const dislikesDiff = (reaction?.isDislike === true) ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likesDiff, // diff is taken from the current closure
    dislikeCount: post.dislikeCount === 0 ? 0 : Number(post.dislikeCount) + dislikesDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const reaction = await postService.getPostReaction(postId);
  const result = await postService.dislikePost(postId);
  const dislikesDiff = result?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  const likesDiff = (reaction?.isLike === true) ? -1 : 0;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + dislikesDiff, // diff is taken from the current closure
    likeCount: post.likeCount === 0 ? 0 : Number(post.likeCount) + likesDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.getComment(commentId);
  const reaction = await commentService.getCommentReaction(commentId);
  const result = await commentService.likeComment(commentId);
  const likesDiff = result?.id ? 1 : -1;
  const dislikesDiff = (reaction?.isDislike === true) ? -1 : 0;
  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + likesDiff,
    dislikeCount: Number(comment.dislikeCount) + dislikesDiff
  });
  const mapComments = post => {
    const { comments } = post;
    const updatedComments = comments.map(comment => (comment.id === commentId ? mapLikes(comment) : comment));
    return ({
      ...post,
      comments: updatedComments
    });
  };
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.getComment(commentId);
  const reaction = await commentService.getCommentReaction(commentId);
  const result = await commentService.dislikeComment(commentId);
  const dislikesDiff = result?.id ? 1 : -1;
  const likesDiff = (reaction?.isLike === true) ? -1 : 0;
  const mapDislikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + likesDiff,
    dislikeCount: Number(comment.dislikeCount) + dislikesDiff
  });
  const mapComments = post => {
    const { comments } = post;
    const updatedComments = comments.map(comment => (comment.id === commentId ? mapDislikes(comment) : comment));
    return ({
      ...post,
      comments: updatedComments
    });
  };
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
