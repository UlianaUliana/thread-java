import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import UpdatePost from 'src/components/UpdatePost';
import * as imageService from 'src/services/imageService';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePost } from 'src/containers/Thread/actions';

import styles from './styles.module.scss';

const UpdatedPost = ({
  posts,
  postId,
  close
}) => {
  const uploadImage = file => imageService.uploadImage(file);
  const post = posts.find(item => item.id === postId);
  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Update Post</span>
      </Modal.Header>
      <Modal.Content>
        <UpdatePost post={post} postId={postId} updatePost={updatePost} uploadImage={uploadImage} close={close} />
      </Modal.Content>
    </Modal>
  );
};

UpdatedPost.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

UpdatedPost.defaultProps = {
  posts: []
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts
});

const actions = { updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatedPost);

